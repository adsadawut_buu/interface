/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.animal;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.run();
        h1.walk();
        h1.sleep();
        System.out.println("h1 is Animal ? " + (h1 instanceof Animal));
        System.out.println("h1 is LandAnimal ? " + (h1 instanceof LandAnimal));

        Animal a1 = h1;
        System.out.println("a1 is LandAnimal ? " + (a1 instanceof LandAnimal));
        System.out.println("a1 is Reptile ? " + (a1 instanceof Reptile));
    
        Cat c1 = new Cat("Dum");
        c1.eat();
        c1.sleep();
        c1.speak();
        c1.run();
        System.out.println("c1 is Animal ? " + (c1 instanceof Animal));
        System.out.println("c1 is LandAnimal ? " + (c1 instanceof LandAnimal));

        Animal a2 = c1;
        System.out.println("a2 is LandAnimal ? " + (a2 instanceof LandAnimal));
        System.out.println("a2 is Poultry ? " + (a2 instanceof Poultry));
        
        Dog d1 = new Dog("Coco");
        d1.eat();
        d1.sleep();
        d1.speak();
        d1.run();
        System.out.println("d1 is Animal ? " + (d1 instanceof Animal));
        System.out.println("d1 is LandAnimal ? " + (d1 instanceof LandAnimal));
        
        Animal a3 = d1;
        System.out.println("a3 is LandAnimal ? " + (a3 instanceof LandAnimal));
        System.out.println("a3 is AquaticAnimal ? " + (a3 instanceof AquaticAnimal));
        
        Alligator al1 = new Alligator("Charawan");
        al1.walk();
        al1.eat();
        al1.crawl();
        al1.sleep();
        System.out.println("al1 is Animal ? " + (al1 instanceof Animal));
        System.out.println("al1 is Reptile ? " + (al1 instanceof Reptile));
        
        Animal a4 = al1;
        System.out.println("a4 is Reptile ? " + (a4 instanceof Reptile));
        System.out.println("a4 is AquaticAnimal ? " + (a4 instanceof AquaticAnimal));
        
        Snake s1 =new Snake("Lam");
        s1.eat();
        s1.sleep();
        s1.crawl();
        s1.walk();
        System.out.println("s1 is Animal ? " + (s1 instanceof Animal));
        System.out.println("s1 is Reptile ? " + (s1 instanceof Reptile));
        
         Animal a5 = s1;
        System.out.println("a5 is Reptile ? " + (a5 instanceof Reptile));
        System.out.println("a5 is Poultry ? " + (a5 instanceof Poultry));
        
        Fish f1 = new Fish("Thong");
        f1.eat();
        f1.sleep();
        f1.walk();
        f1.swim();
        System.out.println("f1 is Animal ? " + (f1 instanceof Animal));
        System.out.println("f1 is AquaticAnimal ? " + (f1 instanceof AquaticAnimal));
        
        Animal a6 = f1;
        System.out.println("a6 is AquaticAnimal ? " + (a6 instanceof AquaticAnimal));
        System.out.println("a6 is Reptile ? " + (a6 instanceof Reptile));
        
        Crab cr1= new Crab("Orange");
        cr1.walk();
        cr1.eat();
        cr1.swim();
        cr1.sleep();
        System.out.println("cr1 is Animal ? " + (cr1 instanceof Animal));
        System.out.println("cr1 is AquaticAnimal ? " + (cr1 instanceof AquaticAnimal));
        
        Animal a7 = cr1;
        System.out.println("a7 is AquaticAnimal ? " + (a7 instanceof AquaticAnimal));
        System.out.println("a7 is LandAnimal ? " + (a7 instanceof LandAnimal));
        
        Bat b1 = new Bat("Badman");
        b1.eat();
        b1.walk();
        b1.speak();
        b1.fly();
        System.out.println("b1 is Animal ? " + (b1 instanceof Animal));
        System.out.println("b1 is Poultry ? " + (b1 instanceof Poultry));
        
        Animal a8 = b1;
        System.out.println("a8 is Poultry ? " + (a8 instanceof Poultry));
        System.out.println("a8 is Reptile ? " + (a8 instanceof Reptile));
        
        Bird bd1 = new Bird("John");
        bd1.fly();
        bd1.walk();
        bd1.speak();
        bd1.sleep();
        System.out.println("bd1 is Animal ? " + (bd1 instanceof Animal));
        System.out.println("bd1 is Poultry ? " + (bd1 instanceof Poultry));
        
        Animal a9 = bd1;
        System.out.println("a9 is Poultry ? " + (a9 instanceof Poultry));
        System.out.println("a9 is LandAnimal ? " + (a9 instanceof LandAnimal));
        System.out.println("Finish The Project!!!");
    }
}
