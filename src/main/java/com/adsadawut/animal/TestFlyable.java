/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.animal;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class TestFlyable {
    public static void main(String[] args) {
        Plane plane1= new Plane("Engine Number 1");
        plane1.fly();
        Bat bat1 = new Bat("Black");
        bat1.fly();
        
        Flyable[] flyable={bat1,plane1};
        for(Flyable f:flyable){
            if(f instanceof Plane){
                 Plane p = (Plane)f;
                 p.startEngine();
                 p.run();
            }
            f.fly();
        }
        
        Dog dog1 = new Dog("Dang");
        dog1.run();
        Car car1 = new Car("RB26");
        car1.run();
        
        Runable[] runable = {plane1,dog1,car1};
        for(Runable r:runable){
            if(r instanceof Plane){
                Plane  p = (Plane)r;
                p.startEngine();
                p.raiseSpeed();
            }
            if(r instanceof Car){
                Car c = (Car)r;
                c.startEngine();
                c.raiseSpeed();
            }
            r.run();
        }
    }
}
