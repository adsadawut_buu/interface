/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.animal;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class Car extends Vehicle implements Runable{
    public Car(String engine){
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println("Car : "+ engine +" start engine");
    }

    @Override
    public void stopEngine() {
        System.out.println("Car : "+ engine +" stop engine" );
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Car : "+ engine +" raise speed" );
    }

    @Override
    public void applyBreak() {
        System.out.println("Car : "+ engine +" apply break" );
    }

    @Override
    public void run() {
        System.out.println("Car : "+ engine +" run" );
    }
    
}
