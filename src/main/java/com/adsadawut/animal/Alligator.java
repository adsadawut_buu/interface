/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.animal;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class Alligator extends Reptile{
    private String nickname;

    public Alligator(String nickname) {
        super("Alligator", 4);
        this.nickname = nickname;
    }
    

    @Override
    public void crawl() {
        System.out.println("Alligator : " + nickname + " crawl");
    }

    @Override
    public void eat() {
        System.out.println("Alligator : " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Alligator : " + nickname + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Alligator : " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Alligator : " + nickname + " sleep");
    }
}
